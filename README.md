# API RESTFull desenvolvida como Prova de conhecimento para a vaga de desenvolvedor - MV


<br>

# Professional
### List All Professionals
```[GET] https://api-mv.herokuapp.com/api/professional/ ``` <br>

### List By Id
```[GET] https://api-mv.herokuapp.com/api/professional/{id} ``` <br>

### Save new Professional
```[POST] https://api-mv.herokuapp.com/api/professional/ ``` <br>
```JSON
{
	"name":"nameProfessional",
	"phoneNumber" : "+55 00 0 0000 0000",
	"residentialPhoneNumber" : "+00 00 0 0000 0000",
	"function" : "functionProfessional",
	"address" : {
		"street" : "streetProfessional",
		"number" : 0,
		"city" : "cityProfessinal",
		"district" : "districtProfessional",
		"zipCode" : "00000-000"
	}
}
```
### Update an Professional
```[PUT] https://api-mv.herokuapp.com/api/professional/{id} ``` <br>
```JSON
{
	"name":"nameProfessional",
	"phoneNumber" : "+55 00 0 0000 0000",
	"residentialPhoneNumber" : "+00 00 0 0000 0000",
	"function" : "functionProfessional",
	"address" : {
		"street" : "streetProfessional",
		"number" : 0,
		"city" : "cityProfessinal",
		"district" : "districtProfessional",
		"zipCode" : "00000-000"
	}
}
```
### Delete an Professional
```[DELETE] https://api-mv.herokuapp.com/api/professional/{id} ```
___
<br> <br>

# Establishment

### List All Establishments
```[GET] https://api-mv.herokuapp.com/api/establishment/ ``` <br>

### List By ID
```[GET] https://api-mv.herokuapp.com/api/establishment/{id} ``` <br>

### Save new Establishment
```[POST] https://api-mv.herokuapp.com/api/establishment/ ``` <br>
```JSON
{
	"name":"nameEstablishment",
	"phoneNumber" : "+00 00 0 0000 0000",
	"address" : {
		"street" : "sreetEstabishment",
		"number" : 0,
		"city" : "cityEstablishment",
		"district" : "districtEstablishment",
		"zipCode" : "00000-000"
	}
}
```
### Update an Establishment
```[PUT] https://api-mv.herokuapp.com/api/establishment/{id} ``` <br>
```JSON
{
	"name":"nameEstablishment",
	"phoneNumber" : "+00 00 0 0000 0000",
	"address" : {
		"street" : "sreetEstabishment",
		"number" : 0,
		"city" : "cityEstablishment",
		"district" : "districtEstablishment",
		"zipCode" : "00000-000"
	}
}
```
### Delete an Establishment
```[DELETE] https://api-mv.herokuapp.com/api/establishment/{id} ``` 
<br>

***

<br> <br>

# Allocateds
### List All Allocateds
```[GET] https://api-mv.herokuapp.com/api/allocated/ ``` <br>

### List By ID
```[GET] https://api-mv.herokuapp.com/api/allocated/{id} ``` <br>

### Save new Allocated
```[POST] https://api-mv.herokuapp.com/api/allocated/{idEstablishment}/{idProfessional} ``` <br>

### Update an Allocated
```[PUT] https://api-mv.herokuapp.com/api/allocated/{id}/{idEstablishment}/{idProfessional} ``` <br>

### Delete an Allocated
```[DELETE] https://api-mv.herokuapp.com/api/allocated/{id} ``` <br>
***
<br> <br>

