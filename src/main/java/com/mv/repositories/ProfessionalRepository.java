package com.mv.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mv.entities.Professional;

@Repository
public interface ProfessionalRepository extends JpaRepository<Professional, Long> {

}
