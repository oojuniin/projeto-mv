package com.mv.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mv.entities.Allocated;

@Repository
public interface AllocatedRepository extends JpaRepository<Allocated, Long> {

}
