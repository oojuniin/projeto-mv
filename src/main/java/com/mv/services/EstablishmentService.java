package com.mv.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mv.entities.Establishment;
import com.mv.repositories.EstablishmentRepository;

@Service
public class EstablishmentService {

	@Autowired
	private EstablishmentRepository repository;

	@Transactional(readOnly = true)
	public List<Establishment> findAll() {
		return repository.findAll();
	}

	@Transactional(readOnly = true)
	public Optional<Establishment> findById(Long id) {
		return repository.findById(id);
	}

	@Transactional
	public Establishment save(Establishment establishment) {
		return repository.save(establishment);
	}

	@Transactional
	public Establishment update(Establishment establishment, Long id) {
		Optional<Establishment> establishmentFromDB = findById(id);

		if (establishmentFromDB.isPresent()) {
			Establishment update = establishmentFromDB.get();
			update.setName(establishment.getName());
			update.setPhoneNumber(establishment.getPhoneNumber());
			update.setAddress(establishment.getAddress());
			return save(update);
		}
		return establishmentFromDB.get();
	}

	@Transactional
	public void delete(Long id) {
		Optional<Establishment> establishmentFromDB = findById(id);
		if (establishmentFromDB.isPresent()) {
			repository.deleteById(id);
		}
	}
}