package com.mv.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mv.entities.Allocated;
import com.mv.entities.Establishment;
import com.mv.entities.Professional;
import com.mv.repositories.AllocatedRepository;

@Service
public class AllocatedService {

	@Autowired
	private AllocatedRepository repository;

	@Autowired
	private EstablishmentService establishmentService;

	@Autowired
	private ProfessionalService professionalService;

	@Transactional(readOnly = true)
	public List<Allocated> findAll() {
		return repository.findAll();
	}

	@Transactional(readOnly = true)
	public Optional<Allocated> findById(Long id) {
		return repository.findById(id);
	}

	@Transactional
	public Allocated save(Long idEstablishment, Long idProfessional) {
		Establishment establishment = establishmentService.findById(idEstablishment).get();
		Professional professional = professionalService.findById(idProfessional).get();
		Allocated allocated = new Allocated(null, establishment.getName(), professional.getName());
		return repository.save(allocated);
	}

	@Transactional
	public Allocated update(Long id, Long idEstablishment, Long idProfessional) {
		Optional<Allocated> allocatedFromDB = findById(id);
		if (allocatedFromDB.isPresent()) {
			Allocated update = allocatedFromDB.get();
			Establishment establishment = establishmentService.findById(idEstablishment).get();
			Professional professional = professionalService.findById(idProfessional).get();
			update.setNameEstablishment(establishment.getName());
			update.setNameProfessional(professional.getName());
			return repository.save(update);
		}
		return allocatedFromDB.get();
	}

	@Transactional
	public void delete(Long id) {
		Optional<Allocated> establishmentFromDB = findById(id);
		if (establishmentFromDB.isPresent()) {
			repository.deleteById(id);
		}
	}
}
