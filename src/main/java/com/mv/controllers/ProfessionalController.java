package com.mv.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mv.entities.Professional;
import com.mv.services.ProfessionalService;

@RestController
@RequestMapping("/api/professional")
public class ProfessionalController {

	@Autowired
	private ProfessionalService service;

	@GetMapping("/")
	public ResponseEntity<List<Professional>> findAll() {
		return ResponseEntity.status(HttpStatus.OK).body(service.findAll());
	}

	@GetMapping("/{id}")
	public ResponseEntity<Professional> findById(@PathVariable Long id) {
		if (professionalFromDB(id).isEmpty()) {
			return notFound();
		}
		return ResponseEntity.status(HttpStatus.OK).body(professionalFromDB(id).get());
	}

	@PostMapping
	public ResponseEntity<Professional> save(@RequestBody Professional professional) {
		service.save(professional);
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}

	@PutMapping("/{id}")
	public ResponseEntity<Professional> update(@RequestBody Professional professional, @PathVariable Long id) {
		if (professionalFromDB(id).isEmpty()) {
			return notFound();
		}
		service.update(professional, id);
		return ResponseEntity.status(HttpStatus.OK).build();
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Professional> delete(@PathVariable Long id) {
		if (professionalFromDB(id).isEmpty()) {
			return notFound();
		}
		service.delete(id);
		return ResponseEntity.status(HttpStatus.OK).build();
	}

	private ResponseEntity<Professional> notFound() {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}

	private Optional<Professional> professionalFromDB(Long id) {
		Optional<Professional> professionalFromDB = service.findById(id);
		return professionalFromDB;
	}
}
