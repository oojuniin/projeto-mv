package com.mv.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mv.entities.Allocated;
import com.mv.services.AllocatedService;

@RestController
@RequestMapping("/api/allocated")
public class AllocatedController {

	@Autowired
	private AllocatedService service;

	@GetMapping("/")
	public ResponseEntity<List<Allocated>> findAll() {
		return ResponseEntity.status(HttpStatus.OK).body(service.findAll());
	}

	@GetMapping("/{id}")
	public ResponseEntity<Allocated> findById(@PathVariable Long id) {
		if (allocatedFromDB(id).isEmpty()) {
			return notFound();
		}
		return ResponseEntity.status(HttpStatus.OK).body(allocatedFromDB(id).get());
	}

	@PostMapping("/{idEstableshiment}/{idProfessional}")
	public ResponseEntity<Allocated> save(@PathVariable Long idEstableshiment,
			@PathVariable Long idProfessional) {
		service.save(idEstableshiment, idProfessional);
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}

	@PutMapping("/{id}/{idEstableshiment}/{idProfessional}")
	public ResponseEntity<Allocated> update(@PathVariable Long id, @PathVariable Long idEstableshiment,
			@PathVariable Long idProfessional) {
		if (allocatedFromDB(id).isEmpty()) {
			return notFound();
		}
		service.update(id, idEstableshiment, idProfessional);
		return ResponseEntity.status(HttpStatus.OK).build();
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Allocated> delete(@PathVariable Long id) {
		if (allocatedFromDB(id).isEmpty()) {
			return notFound();
		}
		service.delete(id);
		return ResponseEntity.status(HttpStatus.OK).build();
	}

	private ResponseEntity<Allocated> notFound() {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}

	private Optional<Allocated> allocatedFromDB(Long id) {
		Optional<Allocated> allocatedFromDB = service.findById(id);
		return allocatedFromDB;
	}
}
